/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example/alarm.ds.ts        */
/*    -------------------------------------------------------------    */
/*    Author      :  Manuel Serrano                                    */
/*    Creation    :  Thu Apr 21 08:55:40 2022                          */
/*    Last change :  Thu Apr 21 13:41:20 2022 (serrano)                */
/*    Copyright   :  2022 Manuel Serrano                               */
/*    -------------------------------------------------------------    */
/*    Zigbee alarm interface                                           */
/*=====================================================================*/

/*---------------------------------------------------------------------*/
/*    The module                                                       */
/*---------------------------------------------------------------------*/
import { Client, packetTypeName } from "..";
import { ReactiveMachine } from "@hop/hiphop";

/*---------------------------------------------------------------------*/
/*    types                                                            */
/*---------------------------------------------------------------------*/
export interface AlarmOptions {
   zigbeeBase?: string,
   connectTimeout?: number
}

export interface Devices {
   motions: string[],
   winsensors: string [],
   plugs: string [],
   switches: string []
}

export interface ConnectSignal {
   connect: boolean
}

export interface MessageSignal {
   topic: string,
   payload: any
}

export interface AlarmSignal {
   count: number,
   date: Date
}

export interface ErrorSignal {
   reason: "connectTimeout" | "subscribeTimeout",
   object?: any
}

/*---------------------------------------------------------------------*/
/*    reactive machine                                                 */
/*---------------------------------------------------------------------*/
export type AlarmMachine = 
	       ReactiveMachine<connect: ConnectSignal, message: messageSignal, alarm: alarmSignal, error: errorSignal, log: string>;

/*---------------------------------------------------------------------*/
/*    constructor                                                      */
/*---------------------------------------------------------------------*/
export function alarm(client: Client, devices: AlarmDevices, opts?: AlarmOptions) : AlarmMachine;
