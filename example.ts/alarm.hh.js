/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example/alarm.hh.js        */
/*    -------------------------------------------------------------    */
/*    Author      :  Manuel Serrano                                    */
/*    Creation    :  Thu Apr 21 08:55:07 2022                          */
/*    Last change :  Sun May 22 07:16:43 2022 (serrano)                */
/*    Copyright   :  2022 Manuel Serrano                               */
/*    -------------------------------------------------------------    */
/*    A Zigbee alarm                                                   */
/*=====================================================================*/
"use @hop/hiphop";

/*---------------------------------------------------------------------*/
/*    Module                                                           */
/*---------------------------------------------------------------------*/
import { promise } from "@hop/hiphop/modules/promise.hh.js";
import { interval } from "@hop/hiphop/modules/interval.hh.js";
import { timeout } from "@hop/hiphop/modules/timeout.hh.js";
export { alarm };

/*---------------------------------------------------------------------*/
/*    AlarmInterface ...                                               */
/*---------------------------------------------------------------------*/
hiphop interface AlarmInterface {
   in connect;
   in message;
   in emergency;
   out alarm;
   out error;
}

/*---------------------------------------------------------------------*/
/*    global parameters and constants                                  */
/*---------------------------------------------------------------------*/
const ZIGBEE_DEVICES_MESSAGE_TOPIC = "zigbee2mqtt/bridge/devices";
let ZIGBEEBASE = "zigbee2mqtt";

const CONNTIMEOUT = 6;
const ALARMTIMEOUT = 10;
const ALARMDURATION = 5;

/*---------------------------------------------------------------------*/
/*    zigbeebase ...                                                   */
/*---------------------------------------------------------------------*/
function zigbeeBase(name) { 
   return  ZIGBEEBASE + "/" + name;
}

/*---------------------------------------------------------------------*/
/*    log ...                                                          */
/*---------------------------------------------------------------------*/
function log(msg) {
   console.log("[" + new Date() + "]", msg);
}
   
/*---------------------------------------------------------------------*/
/*    motionSensors ...                                                */
/*---------------------------------------------------------------------*/
function motionSensors(devices) {
   const allSensors = {};
   
   devices.motions.forEach(name => allSensors[name] = true);
   
   return hiphop module motionSensors() {
      in message;
      out motions = allSensors;
       
      emit motions(allSensors);
      
      fork ${devices.motions.map(m => {
         /// for each motion detector wait for an event
      	 const base = zigbeeBase(m);
      	 
      	 return hiphop {
	    every (message.nowval && message.nowval.topic === base) {
	       if (allSensors[m] !== message.nowval.payload.occupancy) {
	       	  host { allSensors[m] = message.nowval.payload.occupancy; }
	       	  emit motions(allSensors);
	       }
	    }
      	 }
      })}
   }
}
       
/*---------------------------------------------------------------------*/
/*    windowSensors ...                                                */
/*---------------------------------------------------------------------*/
function windowSensors(devices) {
   const allSensors = {};
   
   devices.windows.forEach(name => allSensors[name] = false);
   
   return hiphop module windowSensors() {
      in message;
      out windows = allSensors;
       
      emit windows(allSensors);
      
      fork ${devices.windows.map(w => {
         /// for each window detector wait for an event
      	 const base = zigbeeBase(w);
      	 
      	 return hiphop {
	    every (message.nowval && message.nowval.topic === base) {
	       if (allSensors[w] !== message.nowval.payload.contact) {
	       	  host { allSensors[w] = message.nowval.payload.contact; }
	       	  emit windows(allSensors);
	       }
	    }
      	 }
      })}
   }
}
       
/*---------------------------------------------------------------------*/
/*    alarmOnOff ...                                                   */
/*---------------------------------------------------------------------*/
function alarmOnOff(devices) {
   return hiphop module() {
      out alarmOn, alarmOff;
      in message;
   
      // wait for any of the alarm switch to be turned on
      fork ${devices.switches.map(s => hiphop {
         ///						
      	 const base = zigbeeBase(s.name);
      
      	 every (message.now && message.nowval.topic === base) {
	    if (s.on(message.nowval.payload)) {
	       emit alarmOn();
	    } else {
	       emit alarmOff();
	    }
      	 }
      })}
   }
}
   
/*---------------------------------------------------------------------*/
/*    subscriptions ...                                                */
/*---------------------------------------------------------------------*/
function subscriptions(client, devices) {
   return hiphop module() {
       fork ${devices.windows.map(w => 
		hiphop run promise(client.subscribe(zigbeeBase(w))) { * })};
       fork ${devices.motions.map(m => 
		hiphop run promise(client.subscribe(zigbeeBase(m))) { * })};
       fork ${devices.switches.map(s => 
		hiphop run promise(client.subscribe(zigbeeBase(s.name))) { * })}
   }
}

/*---------------------------------------------------------------------*/
/*    alarmTimeout ...                                                 */
/*---------------------------------------------------------------------*/
hiphop module alarmTimeout(timeout) {
   signal elapsed;
   
   fork {
      run interval(timeout * 1000, 1000) { * };
   } par {
      weakabort (elapsed.nowval >= timeout * 1000) {
	 every (elapsed.now) {
	    host { log("alarm in " + (((timeout * 1000) - elapsed.nowval) / 1000) + "s"); }
	 }
      }
   }
}

/*---------------------------------------------------------------------*/
/*    setAlarm ...                                                     */
/*---------------------------------------------------------------------*/
function setAlarm(client, devices) {
   return hiphop module(val) {
       fork ${devices.alarms.map(a => 
                hiphop run promise(client.publish(zigbeeBase(a) + "/set", { state: val ? "ON" : "OFF" })) { * })}
   }
}

/*---------------------------------------------------------------------*/
/*    checkDevices ...                                                 */
/*---------------------------------------------------------------------*/
function checkDevices(client, devices) {
   
   function missingDevices(payload) {
      const missing = [];
      console.lg("payload=", payload);
      devices.forEach(devname => {
         const d = payload.find(d => d.friendly_name === devname);
	 
	 if (!d) {
	    missing.push(d);
	 } else {
	    console.log("d=", d);
         }
      });
      return missing.length > 0 ? missing : false;
   }
   
   return hiphop module() {
      out error;
      in message;
      
      host { client.subscribe(ZIGBEE_DEVICES_MESSAGE_TOPIC); }
      await (message.now && message.nowval.topic === ZIGBEE_DEVICES_MESSAGE_TOPIC);
							     
      if (missingDevices(message.nowval.payload)) {
	 emit error({ reason: "devices not found", payload: (missingDevices(devices, message.nowval.payload))});
      }
   }
}

/*---------------------------------------------------------------------*/
/*    alarm ...                                                        */
/*---------------------------------------------------------------------*/
function alarm(client, devices, opts) {
   
   ZIGBEEBASE = opts?.zigbeeBase ?? "zigbee2mqtt";
   
   return hiphop module alarmMachine() implements AlarmInterface {
      signal value;
      signal timeout;
      const connectTimeout = opts?.connectTimeout ?? CONNTIMEOUT;
      const alarmTimeout = opts?.alarmTimeout ?? ALARMTIMEOUT;
      const alarmDuration = opts?.alarmDuration ?? ALARMDURATION;

      // DOUBLE ABORT DONC CA NE MARCHE PAS !!! (cf connect.now)
      abort (error.now) {
	 signal alarmOn, alarmOff;
	 signal motions, windows;
	 
      	 // wait for connection
	 // DOUBLE ABORT DONC CA NE MARCHE PAS !!!
      	 abort (connect.now) {
	    run timeout(connectTimeout * 1000) { * };
	    emit error({ reason: "connectTimeout" });
	 }
	 host { log("connected"); }
	 
	 // check devices
      	 devicesChecked: {
	    fork {
	       run timeout(connectTimeout * 1000) { * };
               emit error({ reason: "devicesMissing" });
	    } par {
	       run ${checkDevices(client, devices)}() { * };
               break devicesChecked;
 	    }
	 }
	 host { log("devices checked"); }
	 
	 // subscribe to the motions, winsensors, and switches
/* 	 run ${subscriptions(client, devices)}() { + };                */
/* 	 host { log("subscribed"); }                                   */
/* 	                                                               */
/*       	 fork {                                                */
/* 	    run ${alarmOnOff(devices)}() { + };                        */
/* 	 } par {                                                       */
/* 	    run ${motionSensors(devices)}() { + };                     */
/* 	 } par {                                                       */
/* 	    run ${windowSensors(devices)}() { + };                     */
/*       	 } par {                                               */
/* 	    // enter the infinite loop                                 */
/* 	    do {                                                       */
/* 	       signal warning;                                         */
/* 	                                                               */
/* 	       // switch off the alarm                                 */
/* 	       run ${setAlarm(client, devices)}(false) {};             */
/* 	                                                               */
/* 	       // wait for the alarm to be switched on                 */
/* 	       host { log("waiting for user activating alarm..."); }   */
/* 	       await (alarmOn.now);                                    */
/* 	       // waiting for the room to be clear                     */
/*                host { log("alarm activated") }                      */
/* 	                                                               */
/* 	       // check the windows                                    */
/* 	       if (!(Object.keys(windows.nowval).every(k => windows.nowval[k]))) { */
/* 		  host { log("windows open: " +                        */
/* 			    Object.keys(windows.nowval)                */
/*                                .filterMap(k => !(windows.nowval[k]) ? k : false) */
/* 			       .join(", ")) };                         */
/* 		  await (Object.keys(windows.nowval).every(k => windows.nowval[k])); */
/* 	       }                                                       */
/* 	       host { log("all windows closed"); }                     */
/* 	                                                               */
/* 	       if (!(Object.keys(motions.nowval).every(k => !(motions.nowval[k])))) { */
/* 		  host { log("waiting for occupancy cleared..."); }    */
/* 	       	  await immediate (Object.keys(motions.nowval).every(k => !(motions.nowval[k]))); */
/* 	       }                                                       */
/*  	       host { log("alarm on"); }                               */
/* 	                                                               */
/* 	       emit firstIntrusion(true);                              */
/* 	       // waiting for a motion to be detected                  */
/* 	       loop {                                                  */
/* 		  // switch off the alarm again                        */
/* 	       	  run ${setAlarm(client, devices)}(false) {};          */
/* 		                                                       */
/* 	       	  await ((Object.keys(motions.nowval).every(k => motions.nowval[k])) */
/* 		         || (firstIntrusion.nowval && (Object.keys(windows.nowval).find(k => !(windows.nowval[k]))))); */
/* 	       	  host { log("motion observed!"); }                    */
/* 	                                                               */
/* 	       	  emit alarm(new Date());                              */
/* 		                                                       */
/* 		  if (firstIntrusion.nowval) {                         */
/* 	       	     run alarmTimeout(alarmTimeout) {};                */
/* 		  }                                                    */
/* 	       	                                                       */
/* 		  // switch on the alarm                               */
/* 	       	  host { log("intrusion detected!"); }                 */
/* 		  run ${setAlarm(client, devices)}(true) {};           */
/* 		  await (Object.keys(motions.nowval).every(k => !(motions.nowval[k]))); */
/* 		  emit firstIntrusion(false);                          */
/* 	       }                                                       */
/*             } every (alarmOff.now || emergency.now)                 */
/* 	 } par {                                                       */
/* 	    every (alarmOff.now || emergency.now) {                    */
/* 	       host { log("alarm off"); }                              */
/*       	    }                                                  */
/* 	 }                                                             */
      }
   }
}
