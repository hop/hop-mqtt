/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example/zigbee.hh.ts       */
/*    -------------------------------------------------------------    */
/*    Author      :  Manuel Serrano                                    */
/*    Creation    :  Sat Apr  9 09:39:44 2022                          */
/*    Last change :  Fri Apr 15 09:00:41 2022 (serrano)                */
/*    Copyright   :  2022 Manuel Serrano                               */
/*    -------------------------------------------------------------    */
/*    Zigbee mqtt + hiphop example                                     */
/*=====================================================================*/
"use hopscript";

/*---------------------------------------------------------------------*/
/*    The module                                                       */
/*---------------------------------------------------------------------*/
import { Client, packetTypeName } from "..";
import { ReactiveMachine } from "@hop/hiphop";
import { PLUG } from "./plug.hh.js";

/*---------------------------------------------------------------------*/
/*    clientOpts                                                       */
/*---------------------------------------------------------------------*/
const clientOpts = {
   clientId: "zigbee_" + Math.random().toString(16).substr(2, 8)
}

/*---------------------------------------------------------------------*/
/*    example                                                          */
/*---------------------------------------------------------------------*/
function example() {
   console.log("creating client@localhost:1883");
   const cl = new Client("localhost:1883", clientOpts);
   const ma = new ReactiveMachine(PLUG(cl, {
      plug: "plug1", 
      temp: "temp1", 
      motion: "motion1",
      switch: "switch1"
   }));

   cl.on('message', (t, m, p) => {
      ma.react({message: { topic: t, payload: JSON.parse(m)}});
   });
   cl.on('connect', pk => ma.react({connect: true}));

   cl.connect();
}

example();
