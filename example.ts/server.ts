/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example/server.ts          */
/*    -------------------------------------------------------------    */
/*    Author      :  manuel serrano                                    */
/*    Creation    :  Tue Mar 29 13:12:23 2022                          */
/*    Last change :  Wed Apr 27 18:30:04 2022 (serrano)                */
/*    Copyright   :  2022 manuel serrano                               */
/*    -------------------------------------------------------------    */
/*    A server example                                                 */
/*=====================================================================*/

/*---------------------------------------------------------------------*/
/*    The module                                                       */
/*---------------------------------------------------------------------*/
import { Zeroconf } from "hop:zeroconf";
import { Server, packetTypeName } from "..";

/*---------------------------------------------------------------------*/
/*    global constant                                                  */
/*---------------------------------------------------------------------*/
const port = 1883;

/*---------------------------------------------------------------------*/
/*    onpacketreceive                                                  */
/*---------------------------------------------------------------------*/
function onpacketreceive(packet) {
   console.log("### onpacketreceive:", packetTypeName(packet.type), packet.topic || packet.payload);
}

/*---------------------------------------------------------------------*/
/*    onconnect ...                                                    */
/*---------------------------------------------------------------------*/
async function onconnect(clientid) {
   console.log("### new client", clientid);
}

/*---------------------------------------------------------------------*/
/*    ondisconnect ...                                                 */
/*---------------------------------------------------------------------*/
function ondisconnect(clientid) {
   console.log("### client disconnected", clientid);
}

/*---------------------------------------------------------------------*/
/*    getDevices ...                                                   */
/*---------------------------------------------------------------------*/
async function getDevices() {
   console.log(`creating "server@localhost${port}`);
   
   const zc = Zeroconf();
   const srv = new Server(port, {debug: false});

   srv.on('connect', onconnect);
   srv.on('disconnect', ondisconnect);
   srv.on('packetreceive', onpacketreceive);
   srv.on('publish', (clientId, topic) => console.log("### publish", clientId, topic));

   zc.publish("hopmqtt", "_mqtt._tcp", port);
   
   console.log("server ready...");
   await srv.accept();
}

/*---------------------------------------------------------------------*/
/*    main                                                             */
/*---------------------------------------------------------------------*/
getDevices();
