/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example/hopalarm.ts        */
/*    -------------------------------------------------------------    */
/*    Author      :  manuel serrano                                    */
/*    Creation    :  Tue Mar 29 13:12:23 2022                          */
/*    Last change :  Sun May 22 06:51:59 2022 (serrano)                */
/*    Copyright   :  2022 manuel serrano                               */
/*    -------------------------------------------------------------    */
/*    A Zigbee alarm.                                                  */
/*=====================================================================*/
"use hopscript";

/*---------------------------------------------------------------------*/
/*    The module                                                       */
/*---------------------------------------------------------------------*/
import * as hop from "hop";
import { Zeroconf } from "hop:zeroconf";
import * as syslog from "hop:syslog";

import { ReactiveMachine } from "@hop/hiphop";

import { Server, Client, packetTypeName } from "..";
import { alarm } from "./alarm.hh.js";

/*---------------------------------------------------------------------*/
/*    global variables                                                 */
/*---------------------------------------------------------------------*/
let server: any = false;

/*---------------------------------------------------------------------*/
/*    main                                                             */
/*---------------------------------------------------------------------*/
async function main() {
   const config = require("./config.json") || {};
   config.clientId = "hopalarm_" + Math.random().toString(16).substr(2, 8);

   syslog.open("hopalarm", syslog.LOG_PID | syslog.LOG_ODELAY);
   
   switch (config.mqtt.server || "auto") {
      case "auto":
	 server = await discovery(config.mqtt.type, config.discoveryTimeout)
	    || await mqttServer(config.mqtt.type, config.mqtt.port);
         break;

      case "new":
	 server = await mqttServer(config.mqtt.type, config.mqtt.port);

      default:
	 server = config.mqtt.server;
   }
   
   mqttClient(config, server);
}

/*---------------------------------------------------------------------*/
/*    discovery ...                                                    */
/*---------------------------------------------------------------------*/
function discovery(type, timeout) {
   const zc = Zeroconf();
   const found = false;
   return new Promise((res, rej) => {
      ///		
      let tmt: any = setTimeout(() => { if (tmt) res(false); }, timeout * 1000);
      
      zc.addEventListener("_mqtt._tcp", e => {
	 if (tmt && e.name === "found") {
	    clearTimeout(tmt);
	    tmt = false;
	    res(`${e.hostname}:${e.port}`);
	 }
      })});
}

/*---------------------------------------------------------------------*/
/*    mqttServer ...                                                   */
/*---------------------------------------------------------------------*/
async function mqttServer(type: string, port: number) {
   const zc = Zeroconf();
   const srv = new Server(port);

   srv.on('connect', clientid =>
      syslog.log(syslog.LOG_INFO, "client connected " + clientid));
   srv.on('disconnect', clientid => 
      clientid => syslog.log(syslog.LOG_INFO, "client disconnected " + clientid));
   
   await srv.accept();
   syslog.log(syslog.LOG_INFO, `mqtt:${port} server started`);
   
   zc.publish("hopalarm", type, port);
   
   return srv;

}

/*---------------------------------------------------------------------*/
/*    mqttClient ...                                                   */
/*---------------------------------------------------------------------*/
function mqttClient(config, server) {
   const client = new Client(server, config.clientOpts);
   const mach = new ReactiveMachine(alarm(client, config.devices), { sweep: false });
   
   // bind the mqtt client to the HipHop machine
   client.on('connect', pk => {
      console.log("connect...");
      mach.react({connect: true});
   });
   client.on('message', (t, m, p) => {
      console.log("***", t);
      mach.react({message: { topic: t, payload: JSON.parse(m)}});
   });

   // connect to the MQTT server
   client.connect();
}

main();
