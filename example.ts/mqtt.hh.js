"use @hop/hiphop";

export { P };

hiphop module ZB(cl, plug) {
   inout onmessage;
   
   host { cl.subscribe(plug) };
   
   await immediate(onmessage.now && onmessage.nowval.topic === plug);
   
   host { 
      const msg = JSON.parse(onmessage.nowval.message);
      const state = msg?.state === "ON";
      
      cl.publish(plug + "/set", { state: state ? "OFF" : "ON" }) 
   }
}
