/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example/alarm.ts           */
/*    -------------------------------------------------------------    */
/*    Author      :  manuel serrano                                    */
/*    Creation    :  Tue Mar 29 13:12:23 2022                          */
/*    Last change :  Tue Apr 26 15:20:51 2022 (serrano)                */
/*    Copyright   :  2022 manuel serrano                               */
/*    -------------------------------------------------------------    */
/*    A Zigbee alarm.                                                  */
/*=====================================================================*/
"use hopscript";

/*---------------------------------------------------------------------*/
/*    The module                                                       */
/*---------------------------------------------------------------------*/
import * as hop from "hop";
import { Zeroconf } from "hop:zeroconf";
import * as syslog from "hop:syslog";

import { ReactiveMachine } from "@hop/hiphop";

import { mqttServer } from "./mqtt.ts";
import { Client, packetTypeName } from "..";
import { alarm } from "./alarm.hh.js";
import { discovery } from "./discovery.hh.js";

/*---------------------------------------------------------------------*/
/*    global variables                                                 */
/*---------------------------------------------------------------------*/
let server = false;

/*---------------------------------------------------------------------*/
/*    main                                                             */
/*---------------------------------------------------------------------*/
function main() {
   const config = require("./config.json") || {};
   config.clientId = "hopalarm_" + Math.random().toString(16).substr(2, 8);

   syslog.open("hopalarm", syslog.LOG_PID | syslog.LOG_ODELAY);
   
   switch (config.mqttServer || "auto") {
      case "auto":
	 mqttDiscovery(config);
         break;

      default:
	 if (typeof config.mqttServer === "number") {
	    mqttStart(config);
	 }
   }
}

/*---------------------------------------------------------------------*/
/*    mqttDiscovery ...                                                */
/*---------------------------------------------------------------------*/
function mqttDiscovery(config) {
   server = await discoverServer(_mqtt._tcp, 5);
   
   if (!server) {
      server = await mqttServer(config, zc);
   }
   
   console.log("s=", server);
}
   
/*---------------------------------------------------------------------*/
/*    mqttStart ...                                                    */
/*---------------------------------------------------------------------*/
function mqttStart(config) {
   const client = new Client("localhost:1883", config.clientOpts);
   const mach = new ReactiveMachine(alarm(client, config.devices));

   // bind the mqtt client to the HipHop machine
   client.on('connect', pk => {
      mach.react({connect: true});
   });
   client.on('message', (t, m, p) => {
      console.log("***", t);
      mach.react({message: { topic: t, payload: JSON.parse(m)}});
   });

   // connect to the MQTT server
   client.connect();
}

main();
