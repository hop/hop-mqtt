"use @hop/hiphop";

import { promise } from "@hop/hiphop/modules/promise.hh.js";
import { interval } from "@hop/hiphop/modules/interval.hh.js";
export { PLUG };

function zbbase(name) { 
   return "zigbee2mqtt/" + name;
}

function PLUG(cl, devices) {
   return hiphop module _PLUG() {
      in connect;
      in message;
      signal value;

      // wait for connection
      awaiti (connect.now);      
      	 
      host { console.log("connected...", devices) }
      
      // subscribe to the devices
      fork {
         run promise(cl.subscribe(zbbase(devices.plug))) { * };
      } par {
         run promise(cl.subscribe(zbbase(devices.motion))) { * };
      }

      // initialize...
      host { console.log("waiting for no more moves...") }

      await (message.now 
	     && message.nowval.topic === zbbase(devices.motion)
	     && !(message.nowval.payload.occupancy));

      // set the alarm on
      run promise(cl.publish(zbbase(devices.plug) + "/set", { state: "ON" })) { * };
      host { console.log("alarm on") }
      
      // enter the loop until the alarm is off
      abort (message.now 
	     && message.nowval.topic === zbbase(devices.plug)
 	     && message.nowval.payload.state === "OFF") {
	 signal elapsed, reset, suspend, state;
	    
	 await (message.now && message.nowval.topic === zbbase(devices.motion));
	    
	 // motion detected, start counting
	 fork {
	    run interval(10000, 1000) { * };
	 } par {
	    weakabort (elapsed.nowval >= 10000) {
	       every (elapsed.now) {
	       	  host { console.log("alarm in", ((10000 - elapsed.nowval) / 1000) + "s"); }
	       }
	    }
	 }
	 
	 // intrusion detected
	 loop {
	    signal elapsed, reset, suspend, state;
      	    host { console.log("ALARM ! ALARM ! ALARM !") }
	    run interval(2000, 1000) { * };
	 }
      }
      	 
      // turn the plug off
      run promise(cl.publish(zbbase(devices.plug) + "/set", { state: "OFF" })) { * };
      host { console.log("alarm off") }
   }
}
