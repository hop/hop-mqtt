/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example/devices.ts         */
/*    -------------------------------------------------------------    */
/*    Author      :  manuel serrano                                    */
/*    Creation    :  Tue Mar 29 13:12:23 2022                          */
/*    Last change :  Sat Dec 24 06:02:59 2022 (serrano)                */
/*    Copyright   :  2022 manuel serrano                               */
/*    -------------------------------------------------------------    */
/*    A complete (server+client) example to get available devices      */
/*=====================================================================*/

/*---------------------------------------------------------------------*/
/*    The module                                                       */
/*---------------------------------------------------------------------*/
import { Server, Client, packetTypeName } from "..";

/*---------------------------------------------------------------------*/
/*    clientOpts                                                       */
/*---------------------------------------------------------------------*/
const clientOpts = {
   clientId: "zbDevices_" + Math.random().toString(16).substr(2, 8)
}

/*---------------------------------------------------------------------*/
/*    onpacket                                                         */
/*---------------------------------------------------------------------*/
function onpacket(packet) {
   console.log("+++ onpacket:", packetTypeName(packet.type), packet.pid);
}

/*---------------------------------------------------------------------*/
/*    onpacketreceive                                                  */
/*---------------------------------------------------------------------*/
function onpacketreceive(packet) {
   console.log("### onpacketreceive:", packetTypeName(packet.type), packet.topic || packet.payload || packet);
}

/*---------------------------------------------------------------------*/
/*    onmessage ...                                                    */
/*---------------------------------------------------------------------*/
async function onmessage(topic, message, packet) {
   const self = this;

   if (topic === "zigbee2mqtt/bridge/devices") {
      const devices = JSON.parse(packet.payload);

      // dump devices
      devices.forEach(d => {
	    console.log(d.friendly_name, "(" + d.ieee_address + ")");
	    console.log("   ", d.definition?.description);
	    console.log("   ", d.manufacturer, d.model_id);
	    // console.log("   ", d.endpoints);
	    console.log("");
	 });

      process.exit(0);
   } else {
      console.log(topic, JSON.parse(message));
   }
}

/*---------------------------------------------------------------------*/
/*    onconnect ...                                                    */
/*---------------------------------------------------------------------*/
async function onconnect(clientid) {
   console.log("### new client", clientid);

   if (clientid.match(/mqttjs_/)) {
      // zigbee2mqtt is connected, start our own client
      console.log("creating client@localhost:1883");
      const cl = new Client("localhost:1883", clientOpts);

      cl.on('message', onmessage);
      cl.on('connect', pk => console.log("+++ connected", pk));

      //cl.on('packetreceive', onpacket);

      console.log("+++ connect...");
      await cl.connect();
      console.log("+++ subscribe...");
      await cl.subscribe("zigbee2mqtt/bridge/devices");
      console.log("+++ subscribed...");
   }
}

/*---------------------------------------------------------------------*/
/*    ondisconnect ...                                                 */
/*---------------------------------------------------------------------*/
function ondisconnect(clientid) {
   console.log("### client disconnected", clientid);
}

/*---------------------------------------------------------------------*/
/*    getDevices ...                                                   */
/*---------------------------------------------------------------------*/
async function getDevices(): Promise<number> {
   console.log("creating server@localhost:1883");
   const srv = new Server(1883, {debug: false});

   srv.on('connect', onconnect);
   srv.on('disconnect', ondisconnect);
   srv.on('packetreceive', onpacketreceive);
   srv.on('publish', (clientId, topic) => console.log("### publish", clientId, topic));

   await srv.accept();

   return new Promise((res, rej) => { res(1) });
}

/*---------------------------------------------------------------------*/
/*    main                                                             */
/*---------------------------------------------------------------------*/
getDevices()
   .then(s => console.log("status=", s))
   .catch(e => console.log("error=", e));
