6 Jan 2023
==========

This is an attempt to implement an MQTT binding for Hop and HipHop. This
is an unstable unfinished prototype.

The Hop server acts as an MQTT server then it is a replacement for any other
MQTT server. It will wait client connections on port 1883 so before
starting the server all running MQTT servers should be stop first. In
particular, if mosquitto is running, it should be stopped first:

```
/etc/init.d/mosquitto stop
```

Then the server should be start with:

```
(cd example.js; hop --no-server server.js)
```

or

```
(cd example.ts; hop --no-server server.ts)
```

To check that a client can connect:

```
(cd example.js; hop -p 0 client.js)
```

or

```
(cd example.ts; hop -p 0 client.ts)
```




