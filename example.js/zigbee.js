/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example/zigbee.js          */
/*    -------------------------------------------------------------    */
/*    Author      :  manuel serrano                                    */
/*    Creation    :  Tue Mar 29 13:12:23 2022                          */
/*    Last change :  Wed Apr 13 12:57:23 2022 (serrano)                */
/*    Copyright   :  2022 manuel serrano                               */
/*    -------------------------------------------------------------    */
/*    An example of simple MQTT client                                 */
/*=====================================================================*/

/*---------------------------------------------------------------------*/
/*    The module                                                       */
/*---------------------------------------------------------------------*/
import { Client, packetTypeName } from "..";

let armed = false;

/*---------------------------------------------------------------------*/
/*    deviceNames ...                                                  */
/*---------------------------------------------------------------------*/
const deviceNames = {
   "0x00124b00246cd6a7": "switch",
   "0x00124b0024c106d9": "plug1",
   "0x00124b0024cc3d6a": "temp1"
}

/*---------------------------------------------------------------------*/
/*    clientOpts                                                       */
/*---------------------------------------------------------------------*/
const clientOpts = {
   clientId: "zigbee_" + Math.random().toString(16).substr(2, 8)
}

/*---------------------------------------------------------------------*/
/*    onpacket                                                         */
/*---------------------------------------------------------------------*/
function onpacket(packet) {
   console.log("onpacket:", packetTypeName(packet.type), packet.pid);
}
      
/*---------------------------------------------------------------------*/
/*    onmessage ...                                                    */
/*---------------------------------------------------------------------*/
async function onmessage(topic, message, packet) {
   const self = this;
   console.log("onmessage:", topic);
   
   if (topic === "zigbee2mqtt/bridge/devices") {
      const devices = JSON.parse(packet.payload);
      
      // renaming devices
      devices.forEach(d => {
	    if ((d.ieee_address in deviceNames)
		&& d.friendly_name !== deviceNames[d.ieee_address]) {
	       self.publish('zigbee2mqtt/bridge/config/rename',
		  { old: d.ieee_address, new: deviceNames[d.ieee_address] });
	    }
	 });

      // dump devices
      devices.forEach(d => {
	    console.log(d.friendly_name, "(" + d.ieee_address + ")");
	    console.log("   ", d.definition?.description);
	    console.log("   ", d.manufacturer, d.model_id);
	    //console.log("   ", d.endpoints);
	    console.log("");
	 });
      
      // binding devices
/*       console.log("unbinding...");                                  */
/*       const pku = await this.publish("zigbee2mqtt/bridge/request/device/unbind", */
/* 	 { from: "switch", to: "plug1"}, { qos: 1 });                  */
/*       console.log("unbinding pk=", pku);                            */
      
/*       // binding devices                                            */
/*       console.log("binding...");                                    */
/*       const pkb = await this.publish("zigbee2mqtt/bridge/request/device/bind", */
/* 	 { from: "switch", to: "plug1"}, { qos: 1 });                  */
/*       console.log("binding pk=", pkb);                              */
      
      console.log("publishing...");
      await this.publish('zigbee2mqtt/plug1/get', { state: "" });
      await this.publish('zigbee2mqtt/temp1', {} );
      console.log("published...");
   } else if (topic === "zigbee2mqtt/plug1") {
      if (!armed) {
	 armed = true;
	 const msg = JSON.parse(message);
      	 // switch on/off
	 console.log("plug1 state:", msg);
	 
      	 if (msg?.state === "ON") {
	    console.log("switch off");
      	    // await this.publish('zigbee2mqtt/plug1/set', { state: "OFF" });
      	 } else {
	    console.log("switch on");
      	    // await this.publish('zigbee2mqtt/plug1/set', { state: "ON" });
      	 }
      }
   } else if (topic === "zigbee2mqtt/temp1") {
      console.log("temp1 state:", JSON.parse(message), new Date());
   } else {
      console.log("pk=", packet);
   }
}

/*---------------------------------------------------------------------*/
/*    example                                                          */
/*---------------------------------------------------------------------*/
async function example() {
   console.log("creating client@localhost:1883");
   const cl = new Client("localhost:1883", clientOpts);
   
   cl.on('message', onmessage);
   cl.on('connect', pk => console.log("connected", pk));
   //cl.on('packetreceive', onpacket);

   console.log("connect...");
   await cl.connect();
   console.log("subscribe...");
   await cl.subscribe("zigbee2mqtt/bridge/devices");
   await cl.subscribe("zigbee2mqtt/plug1");
   await cl.subscribe("zigbee2mqtt/temp1");
   console.log("subscribed...");
/*                                                                     */
/*    console.log("client ending...");                                 */
/*    cl.end();                                                        */
/*    console.log("client ended.");                                    */
}

example();
