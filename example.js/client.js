/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/example.js/client.js       */
/*    -------------------------------------------------------------    */
/*    Author      :  manuel serrano                                    */
/*    Creation    :  Tue Mar 29 13:12:23 2022                          */
/*    Last change :  Fri Jan  6 07:45:28 2023 (serrano)                */
/*    Copyright   :  2022-23 manuel serrano                            */
/*    -------------------------------------------------------------    */
/*    An example of simple MQTT client                                 */
/*=====================================================================*/

/*---------------------------------------------------------------------*/
/*    The module                                                       */
/*---------------------------------------------------------------------*/
import { Client, packetTypeName } from "..";

/*---------------------------------------------------------------------*/
/*    clientOpts                                                       */
/*---------------------------------------------------------------------*/
const clientOpts = {
}

/*---------------------------------------------------------------------*/
/*    onmessage ...                                                    */
/*---------------------------------------------------------------------*/
function onmessage(topic, message, packet) {
   console.log("onmessage:", topic, message);
   console.log("         :", packet);
}

/*---------------------------------------------------------------------*/
/*    onpacket                                                         */
/*---------------------------------------------------------------------*/
function onpacket(packet) {
   console.log("onpacket:", packetTypeName(packet.type), packet.pid);
}
      
/*---------------------------------------------------------------------*/
/*    example                                                          */
/*---------------------------------------------------------------------*/
async function example() {
   console.log("creating client@localhost:1883");
   const cl = new Client("localhost:1883", clientOpts);
   
   cl.on('message', onmessage);
   cl.on('packetreceive', onpacket);
   cl.on('connect', pk => console.log("connected", pk));
   
   const pk = await cl.connect();
   
   const devices = await cl.subscribe("zigbee2mqtt/bridge/devices");

   console.log("devices=", devices);
}

example();
