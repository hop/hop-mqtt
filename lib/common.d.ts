/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/lib/common.d.ts            */
/*    -------------------------------------------------------------    */
/*    Author      :  Manuel Serrano                                    */
/*    Creation    :  Thu Apr  7 08:21:50 2022                          */
/*    Last change :  Sun Apr 24 10:03:31 2022 (serrano)                */
/*    Copyright   :  2022 Manuel Serrano                               */
/*    -------------------------------------------------------------    */
/*    Common types (from mqtt-packet for compatibility).               */
/*=====================================================================*/

export declare type QoS = 0 | 1 | 2

export declare type PacketCmd = 'auth' |
  'connack' |
  'connect' |
  'disconnect' |
  'pingreq' |
  'pingresp' |
  'puback' |
  'pubcomp' |
  'publish' |
  'pubrel' |
  'pubrec' |
  'suback' |
  'subscribe' |
  'unsuback' |
  'unsubscribe'

export declare type UserProperties = {[index: string]: string | string[]}

export interface IPacket {
  cmd: PacketCmd
  messageId?: number
  length?: number
}

export interface IAuthPacket extends IPacket {
  cmd: 'auth'
  reasonCode: number,
  properties?: {
    authenticationMethod?: string,
    authenticationData?: string,
    reasonString?: string,
    userProperties?: UserProperties,
  }
}

export interface IConnectPacket extends IPacket {
  cmd: 'connect'
  clientId: string
  protocolVersion?: 4 | 5 | 3
  protocolId?: 'MQTT' | 'MQIsdp'
  clean?: boolean
  keepalive?: number
  username?: string
  password?: string
  will?: {
    topic: string
    payload: string
    qos?: QoS
    retain?: boolean
    properties?: {
      willDelayInterval?: number,
      payloadFormatIndicator?: number,
      messageExpiryInterval?: number,
      contentType?: string,
      responseTopic?: string,
      correlationData?: string,
      userProperties?: UserProperties
    }
  }
  properties?: {
    sessionExpiryInterval?: number,
    receiveMaximum?: number,
    maximumPacketSize?: number,
    topicAliasMaximum?: number,
    requestResponseInformation?: boolean,
    requestProblemInformation?: boolean,
    userProperties?: UserProperties,
    authenticationMethod?: string,
    authenticationData?: string
  }
}

export interface IPublishPacket extends IPacket {
  cmd: 'publish'
  qos: QoS
  dup: boolean
  retain: boolean
  topic: string
  payload: string | string
  properties?: {
    payloadFormatIndicator?: boolean,
    messageExpiryInterval?: number,
    topicAlias?: number,
    responseTopic?: string,
    correlationData?: string,
    userProperties?: UserProperties,
    subscriptionIdentifier?: number,
    contentType?: string
  }
}

export interface IConnackPacket extends IPacket {
  cmd: 'connack'
  returnCode?: number,
  reasonCode?: number,
  sessionPresent: boolean
  properties?: {
    sessionExpiryInterval?: number,
    receiveMaximum?: number,
    maximumQoS?: number,
    retainAvailable?: boolean,
    maximumPacketSize?: number,
    assignedClientIdentifier?: string,
    topicAliasMaximum?: number,
    reasonString?: string,
    userProperties?: UserProperties,
    wildcardSubscriptionAvailable?: boolean,
    subscriptionIdentifiersAvailable?: boolean,
    sharedSubscriptionAvailable?: boolean,
    serverKeepAlive?: number,
    responseInformation?: string,
    serverReference?: string,
    authenticationMethod?: string,
    authenticationData?: string
  }
}

export interface ISubscription {
  topic: string
  qos: QoS,
  nl?: boolean,
  rap?: boolean,
  rh?: number
}

export interface ISubscribePacket extends IPacket {
  cmd: 'subscribe'
  subscriptions: ISubscription[],
  properties?: {
    reasonString?: string,
    userProperties?: UserProperties
  }
}

export interface ISubackPacket extends IPacket {
  cmd: 'suback',
  reasonCode?: number,
  properties?: {
    reasonString?: string,
    userProperties?: UserProperties
  },
  granted: number[] | Object[]
}

export interface IUnsubscribePacket extends IPacket {
  cmd: 'unsubscribe',
  properties?: {
    reasonString?: string,
    userProperties?: UserProperties
  },
  unsubscriptions: string[]
}

export interface IUnsubackPacket extends IPacket {
  cmd: 'unsuback',
  reasonCode?: number,
  properties?: {
    reasonString?: string,
    userProperties?: UserProperties
  }
}

export interface IPubackPacket extends IPacket {
  cmd: 'puback',
  reasonCode?: number,
  properties?: {
    reasonString?: string,
    userProperties?: UserProperties
  }
}

export interface IPubcompPacket extends IPacket {
  cmd: 'pubcomp',
  reasonCode?: number,
  properties?: {
    reasonString?: string,
    userProperties?: UserProperties
  }
}

export interface IPubrelPacket extends IPacket {
  cmd: 'pubrel',
  reasonCode?: number,
  properties?: {
    reasonString?: string,
    userProperties?: UserProperties
  }
}

export interface IPubrecPacket extends IPacket {
  cmd: 'pubrec',
  reasonCode?: number,
  properties?: {
    reasonString?: string,
    userProperties?: UserProperties
  }
}

export interface IPingreqPacket extends IPacket {
  cmd: 'pingreq'
}

export interface IPingrespPacket extends IPacket {
  cmd: 'pingresp'
}

export interface IDisconnectPacket extends IPacket {
  cmd: 'disconnect',
  reasonCode?: number,
  properties?: {
    sessionExpiryInterval?: number,
    reasonString?: string,
    userProperties?: UserProperties,
    serverReference?: string
  }
}

export declare type Packet = IConnectPacket |
  IPublishPacket |
  IConnackPacket |
  ISubscribePacket |
  ISubackPacket |
  IUnsubscribePacket |
  IUnsubackPacket |
  IPubackPacket |
  IPubcompPacket |
  IPubrelPacket |
  IPingreqPacket |
  IPingrespPacket |
  IDisconnectPacket |
  IPubrecPacket |
  IAuthPacket

export declare type OnPacketCallback = (packet: Packet) => void;
