/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/lib/server.d.ts            */
/*    -------------------------------------------------------------    */
/*    Author      :  Manuel Serrano                                    */
/*    Creation    :  Sat Apr 23 15:34:03 2022                          */
/*    Last change :  Sun Apr 24 10:04:01 2022 (serrano)                */
/*    Copyright   :  2022 Manuel Serrano                               */
/*    -------------------------------------------------------------    */
/*    Server types                                                     */
/*=====================================================================*/

import { IAuthPacket, IConnectPacket, IPublishPacket, IDisconnectPacket, IConnackPacket, Packet, QoS, OnPacketCallback } from './common'

export declare type OnConnectionCallback = (clientId: string) => void;
export declare type OnPublishCallback = (clientId: string, topic: string) => void;
	       
export interface ServerOpts {
   debug?: boolean
}

export declare type OnAcceptCallback = (Server) => void;

export class Server {
   public readonly port: number;
   
   constructor(port?: number, opts?: undefined | ServerOpts);

   accept(callback: OnAcceptCallback) : void;
   accept(): Promise<Server>;
			     
   close() : void;
   
   on(event: "connect", callback: OnConnectionCallback): this;
   on(event: "disconnect", callback: OnConnectionCallback): this;
   on(event: "packetreceive", callback: OnPacketCallback): this;
   on(event: "publish", callback: OnPublishCallback): this;
}
