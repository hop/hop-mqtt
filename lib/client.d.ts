/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/lib/client.d.ts            */
/*    -------------------------------------------------------------    */
/*    Author      :  manuel serrano                                    */
/*    Creation    :  Thu Mar 31 19:30:39 2022                          */
/*    Last change :  Fri Jan  6 07:43:47 2023 (serrano)                */
/*    Copyright   :  2022-23 manuel serrano                            */
/*    -------------------------------------------------------------    */
/*    Client types                                                     */
/*=====================================================================*/

import { IAuthPacket, IConnectPacket, IPublishPacket, IDisconnectPacket, IConnackPacket, Packet, QoS, OnPacketCallback } from './common'
													    
export interface PublishOpts {
   qos: number,
   retain: boolean,
   dup: boolean
}

export interface ClientOpts {
   clientId?: string
}

export interface SubscribeOpts {
   qos?: number
}

export declare type OnConnectCallback = (packet: IConnackPacket) => void;
export declare type OnMessageCallback = (topic: string, payload: string, packet: IPublishPacket) => void;
			      
export class Client {
   public readonly host: string;	     
   public readonly port: number;
   public readonly clientId: string;
   public connected: boolean;
   
   constructor(url: string, opts: undefined | ClientOpts);
   
   connect(callback: OnPacketCallback) : void;
   connect() : Promise<Packet>;
   
   publish(topic: string, message: string, opts: PublishOpts, callback: OnPacketCallback) : void;
   publish(topic: string, message: string, opts?: PublishOpts) : Promise<Packet>;
   
   subscribe(topic: string, opts: SubscribeOpts, callback: OnPacketCallback) ;
   subscribe(topic: string, opts?: SubscribeOpts) : Promise<Packet>;

   on(event: "message", callback: OnMessageCallback): this;
   on(event: "connect", callback: OnConnectCallback): this;
   on(event: "packetreceive", callback: OnPacketCallback): this;
}

export function packetTypeName(ty: number) : string;
