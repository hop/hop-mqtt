/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/lib/client.js              */
/*    -------------------------------------------------------------    */
/*    Author      :  manuel serrano                                    */
/*    Creation    :  Tue Mar 29 12:14:38 2022                          */
/*    Last change :  Sun May 22 06:32:33 2022 (serrano)                */
/*    Copyright   :  2022 manuel serrano                               */
/*    -------------------------------------------------------------    */
/*    Hop MQTT client binding.                                         */
/*=====================================================================*/

/*---------------------------------------------------------------------*/
/*    Module                                                           */
/*---------------------------------------------------------------------*/
import { _makeClient, _connect, _publish, _subscribe, _end } from "./_mqtt.hop";
import * as events from "events";

export { Client, connect };
export { packetTypeName } from "./_mqtt.hop";

/*---------------------------------------------------------------------*/
/*    mqttURLDecode ...                                                */
/*---------------------------------------------------------------------*/
function mqttURLDecode(url, options) {
   if (typeof url === "string") {
      const { host, port } = url.split(":");
      return { host: host || "localhost", port: port || 1883 };
   } else if (options?.servers) {
      return {
	 host: options.servers[0].host || "localhost",
	 port: options.servers[0].port || 1883
      }
   } else {
      return { host: "localhost", port: 1883 };
   }
}

/*---------------------------------------------------------------------*/
/*    Client ...                                                       */
/*---------------------------------------------------------------------*/
class Client extends events.EventEmitter {
   #client = undefined;
   host = "localhost";
   port = 1883;
   connected = false;
   clientId;
   
   constructor(url, opts = undefined) {
      super();
      
      const { host, port } = mqttURLDecode(url, opts);
      const cid = opts?.clientId ?? 'hopmqtt_' + Math.random().toString(16).substr(2, 8);
      
      this.#client = _makeClient(host, port, cid);
      
      events.EventEmitter.call(this);
      
      this.host = host;
      this.port = port;
      this.clientId = cid;
   }
   
   connect(callback) { 
      if (!this.connected) {
	 if (callback) {
	    _connect(this, this.#client, pk => {
	       this.connected = true;
	       this.emit('connect', pk);
	       callback(pk);
	    });
	 } else {
	    return new Promise((res,rej) => 
			 _connect(this, this.#client, pk => {
			    this.connected = true;
			    this.emit('connect', pk);
			    res(pk);
			 }));
      	 }
      } else {
	 throw "client already connected";
      }
   }
   
   publish(topic, message, opts, callback) {
      const msg = typeof message === "object" 
	 ? JSON.stringify(message) : message + "";

      if (this.connected) {
	 if (callback) {
	    _publish(this.#client, topic, msg, opts, callback);
	 } else {
	    return new Promise((res, rej) => 
			 _publish(this.#client, topic, msg, opts, res));
	 }
      } else {
	 throw "client not connected";
      }
   }
   
   subscribe(topic, opts, callback) {
      if (this.connected) {
	 if (callback) {
	    _subscribe(this.#client, topic, opts, callback);
	 } else {
	    return new Promise((res, rej) => 
			 _subscribe(this.#client, topic, opts, 
				    (err, val) => err ? rej(val) : res(val)));
	 }
      } else {
	 throw "client not connected";
      }
   }
   
   end() {
      if (this.#client) {
	 this.connected = false;
      	 _end(this.#client);
	 this.#client = false;
      }
   }
}

/*---------------------------------------------------------------------*/
/*    connect ...                                                      */
/*---------------------------------------------------------------------*/
function connect(url, opts = undefined) {
   const cl = new Client(url, opts);
   
   cl.connect();
   return cl;
}
