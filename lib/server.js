/*=====================================================================*/
/*    serrano/prgm/project/hop/work/hopmqtt/lib/server.js              */
/*    -------------------------------------------------------------    */
/*    Author      :  Manuel Serrano                                    */
/*    Creation    :  Sat Apr 23 14:47:08 2022                          */
/*    Last change :  Tue Apr 26 14:47:11 2022 (serrano)                */
/*    Copyright   :  2022 Manuel Serrano                               */
/*    -------------------------------------------------------------    */
/*    Hop MQTT server binding.                                         */
/*=====================================================================*/

/*---------------------------------------------------------------------*/
/*    Module                                                           */
/*---------------------------------------------------------------------*/
import { _makeServer, _accept } from "./_mqtt.hop";
import * as events from "events";

export { Server };

/*---------------------------------------------------------------------*/
/*    Server ...                                                       */
/*---------------------------------------------------------------------*/
class Server extends events.EventEmitter {
   #server;
   port;
   
   constructor(port = 1883, opts) {
      super();
      
      events.EventEmitter.call(this);	      
      this.#server = _makeServer(this, port, opts);
      this.port = port;
   }
   
   accept(callback) {
      if (callback) {
	 _accept(this, this.#server, callback);
      } else {
	 return new Promise((res, rej) => 
		      _accept(this, this.#server, res));
      }
   }
   
   close() {
      if (this.#server) {
	 _close(this.#server);
      }
   }
}
